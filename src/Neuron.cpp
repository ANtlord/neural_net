#include "../header/Neuron.h"

Neuron::Neuron(string letter, int output_val) {
    name = letter;
    output = output_val;
    weight = 0;

    wxString fname = wxString()<<"memory/"<<name;
    const int SIZE = 30;

    if (access( fname.ToUTF8(), F_OK ) != -1) {
        wxImage * img = new wxImage(wxString()<<fname);
        unsigned char * imgData = img->GetData();
        unsigned short h = img->GetHeight();
        unsigned short w = img->GetWidth();

        for(unsigned int y = 0; y < h; y++) {        // To get every channel
            for(unsigned int x = 0; x < w; x++) {     // by full width
                memory[x][y] = imgData[(x + y * w) * 3];
            }
        }
    }
    else {
        for (int i=0; i<SIZE; ++i) {
            for (int j=0; j<SIZE; ++j) {
                memory[i][j] = 0;
            }
        }    
    }
}

Neuron::~Neuron(){

}
