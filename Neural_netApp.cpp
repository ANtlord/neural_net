/***************************************************************
 * Name:      Neural_netApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2012-06-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "Neural_netApp.h"
#include "Neural_netMain.h"

IMPLEMENT_APP(Neural_netApp);

bool Neural_netApp::OnInit()
{
    Neural_netFrame* frame = new Neural_netFrame(0L, _("wxWidgets Application Template"));
    
    frame->Show();
    return true;
}
