CC=g++ -std=c++0x
CXXFLAGS=$(shell wx-config --cxxflags)
LIBS=$(shell wx-config --libs) -static-libgcc
#AL_LIBS=-lalut -lvorbisfile -logg

PROJECT_FOLDER=$(shell pwd)
TARGET=$(shell basename `pwd`)
SRC_DIR=src/
SOURCES=$(wildcard *.cpp $(SRC_DIR)*.cpp)

OBJ_DIR=obj/Release/
OBJ_SRC_DIR=$(OBJ_DIR)$(SRC_DIR)

vpath %.o $(OBJ_DIR)
OBJECTS=$(addprefix $(OBJ_DIR), $(SOURCES:%.cpp=%.o))

$(OBJ_DIR)%.o: %.cpp
	$(CC) $(CXXFLAGS) -c -o $@ $<

all: $(SOURCES) $(TARGET)
    
$(OBJECTS): | $(OBJ_SRC_DIR)

$(OBJ_SRC_DIR): | $(OBJ_DIR)
	mkdir $@

$(OBJ_DIR):
	mkdir obj
	mkdir $@


$(TARGET): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LIBS) #$(AL_LIBS)


clean:
	rm $(OBJECTS)
