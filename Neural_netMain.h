/***************************************************************
 * Name:      Neural_netMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2012-06-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef Neural_netMAIN_H
#define Neural_netMAIN_H

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include <wx/textdlg.h>
#include <wx/listctrl.h>
#include "Neural_netApp.h"
#include <fstream>
#include <vector>

class Neural_netFrame: public wxFrame
{
    public:
        Neural_netFrame(wxFrame *frame, const wxString& title);
        ~Neural_netFrame();
    private:
        enum
        {
            idMenuQuit = 1000,
            idMenuAbout
        };
        void OnClose(wxCloseEvent& event);
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void onListClick(wxListEvent& event);
        void paintNow(wxPaintEvent &e);
        DECLARE_EVENT_TABLE()

        //wxButton * button;
        wxBitmap * bitmap;
        wxImage * image;
        wxListCtrl * imageListCtrl;
        wxListCtrl * weightListCtrl;

        const char NUM;
        const char * STATIC_DIR;
        unsigned char ** imageVals;
};


#endif // Neural_netMAIN_H
