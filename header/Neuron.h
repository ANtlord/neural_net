#include <string>
#include <dirent.h>
#include <wx/string.h>
#include <wx/image.h>

using namespace std;
class Neuron {
    public:
        Neuron(string letter, int output_val);
        ~Neuron();

        string name;
        unsigned char input[30][30];
        int output;
        int weight;
        unsigned char memory[30][30];

    protected:


    private:


};
