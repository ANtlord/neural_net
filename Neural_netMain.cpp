/***************************************************************
 * Name:      Neural_netMain.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2012-06-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "Neural_netMain.h"
#include "header/Neuron.h"

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__WXMAC__)
        wxbuild << _T("-Mac");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

BEGIN_EVENT_TABLE(Neural_netFrame, wxFrame)
    EVT_CLOSE(Neural_netFrame::OnClose)
    EVT_MENU(idMenuQuit, Neural_netFrame::OnQuit)
    EVT_MENU(idMenuAbout, Neural_netFrame::OnAbout)
END_EVENT_TABLE()

Neural_netFrame::Neural_netFrame(wxFrame *frame, const wxString& title)
    : wxFrame(frame, -1, title), STATIC_DIR("static"), NUM(30)
{
    setlocale(LC_ALL,"Russian");
    wxInitAllImageHandlers();
#if wxUSE_MENUS
    // create a menu bar
    wxMenuBar* mbar = new wxMenuBar();
    wxMenu* fileMenu = new wxMenu(_T(""));
    fileMenu->Append(idMenuQuit, _("&Quit\tAlt-F4"), _("Quit the application"));
    mbar->Append(fileMenu, _("&File"));
    wxMenu* helpMenu = new wxMenu(_T(""));
    helpMenu->Append(idMenuAbout, _("&About\tF1"), _("Show info about this application"));
    mbar->Append(helpMenu, _("&Help"));
    SetMenuBar(mbar);
#endif // wxUSE_MENUS

#if wxUSE_STATUSBAR
    // create a status bar with some information about the used wxWidgets version
    this->CreateStatusBar(2);
    SetStatusText(_("Hello Code::Blocks user!"),0);
    SetStatusText(wxbuildinfo(short_f), 1);
#endif // wxUSE_STATUSBAR
    wxPoint startPoint = wxPoint(this->GetSize().GetWidth() / 2, 2);
    const int w = 95;
    imageListCtrl = new wxListCtrl(this, wxID_ANY, startPoint, wxSize( w, this->GetSize().GetHeight() - 2));

    startPoint = wxPoint(this->GetSize().GetWidth() / 2 + w, 2);
    weightListCtrl = new wxListCtrl(this, wxID_ANY, startPoint, wxSize( w, this->GetSize().GetHeight() - 2));
    this->Show();
    //this->button = new wxButton(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, "button");

    //Connect(this->imageListCtrl,wxEVT_CLICK,
            //(wxObjectEventFunction)&Neural_netFrame::OnQuit);
    imageListCtrl->Connect(wxEVT_COMMAND_LIST_ITEM_SELECTED,
            (wxObjectEventFunction)&Neural_netFrame::onListClick, 0, this);
    Connect(wxEVT_PAINT, (wxObjectEventFunction)&Neural_netFrame::paintNow,
            0,this);

    
    imageVals = new unsigned char*[NUM];
    for (char i=0; i < NUM; ++i) 
        imageVals[i] = new unsigned char[NUM];
    
    /* Get Names vector alphabet */
    vector<string> alphabet;
    DIR *dir;
    wxString path = wxString()<<STATIC_DIR<<"/";
    struct dirent *ent;
    if ((dir = opendir (path.ToAscii())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            if (ent->d_name[0] != '.'){
                alphabet.push_back(ent->d_name);
                cout<<ent->d_name<<"\n";
                wxListItem item;
                item.SetText(wxString()<<ent->d_name);
                imageListCtrl->InsertItem(item);
            }
        }
        closedir (dir);
    }
    cout<<"Count: "<<imageListCtrl->GetItemCount()<<"\n";
    /* End getting */

    /* Get data from image, which there is by path */
    image = new wxImage();
    /* End getting */

    const int TRESHOLD = 120;
    const int WHITECOLOR = 250;
    int max=0, max_n=0;

    //log.open("colors.txt");
    Neuron ** neurons = new Neuron*[alphabet.size()];
    for(int i=0; i<alphabet.size(); ++i) {
        neurons[i] = new Neuron(alphabet[i], 0);

        for (int x=0; x<NUM; ++x) {
            for (int y=0; y<NUM; ++y) {
                unsigned char m = imageVals[x][y];
                unsigned char n = neurons[i]->memory[x][y];

                if ( abs(m-n) < TRESHOLD ) {
                    if (m<WHITECOLOR)
                        neurons[i]->weight += 1;

                    if (m != 0) {
                        if (m < WHITECOLOR) 
                            n = (n + (n + m) / 2) / 2;

                        neurons[i]->memory[x][y] = n;
                    }
                    else if (n != 0) 
                        if (m < WHITECOLOR)
                            n = (n + (n + m) / 2) / 2;

                    //log<<"x: "<<x<<"\ty: "<<y<<"\tn: "<<(int)n<<"\n";
                    neurons[i]->memory[x][y] = n;
                }
            }
        }

        wxListItem item;
        item.SetText(wxString()<<neurons[i]->name<<" = "<<neurons[i]->weight);
        weightListCtrl->InsertItem(item);
        if (neurons[i]->weight > max) {
            max = neurons[i]->weight;
            max_n=i;
        }
    }
    //log.close();
    wxTextEntryDialog*dlg = new wxTextEntryDialog(this, "Диалог", wxString()<<"Это буква: "<<
            neurons[max_n]->name, neurons[max_n]->name, (wxOK | wxCANCEL));

    if (dlg->ShowModal() == wxID_OK) {
        for (int i=0; i<alphabet.size(); ++i)
            if (neurons[i]->name == dlg->GetValue()) {
                max_n = i;
                max = neurons[i]->weight;
            }

        const char CHANNELS = 3;
        unsigned char data[CHANNELS*NUM*NUM];

        for (int y=0; y<NUM; ++y){
            for (int x=0; x<NUM; ++x){
                for (int i=0; i<CHANNELS; ++i){
                    data[( x + y * NUM) * CHANNELS + i] = neurons[max_n]->memory[x][y];
                }
            }
        }
        wxImage * img = new wxImage(NUM, NUM, data);
        img->SaveFile(wxString()<<"memory/"<<neurons[max_n]->name);
    }
}


Neural_netFrame::~Neural_netFrame()
{
}

void Neural_netFrame::OnClose(wxCloseEvent &event)
{
    Destroy();
}

void Neural_netFrame::OnQuit(wxCommandEvent &event)
{
    Destroy();
}

void Neural_netFrame::onListClick(wxListEvent& event)
{
    wxString path = wxString()<<STATIC_DIR<<"/"<<event.GetText();
    image->LoadFile(path);
    *image = image->ConvertToGreyscale();

    ofstream log("log.txt");
    for (int i=0; i<NUM; ++i) {
        for (int j=0; j<NUM; ++j) {
            imageVals[i][j] = image->GetRed(i,j);
            log<<(int)imageVals[i][j];
        }
        log<<"\n";
    }
    log.close();
    Refresh();
}

void Neural_netFrame::paintNow(wxPaintEvent &e){ //отрисовка на панельки
    wxPaintDC dc(this);
    wxBitmap * bg = new wxBitmap(*image);
    dc.DrawBitmap(*bg, 10, 10);
}

void Neural_netFrame::OnAbout(wxCommandEvent &event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}
