/***************************************************************
 * Name:      Neural_netApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2012-06-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef Neural_netAPP_H
#define Neural_netAPP_H

#include <wx/app.h>

class Neural_netApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // Neural_netAPP_H
